const prodPlugins = []; //去除log输出
if (process.env.NODE_ENV === 'production') {
  prodPlugins.push('transform-remove-console');
}

module.exports = {
  presets: ['@vue/cli-plugin-babel/preset'],
  plugins: [...prodPlugins],
};
