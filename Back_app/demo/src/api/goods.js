//  商品的接口
import http from "./http";

const getGoodsList = async (obj) => {
  let data = await http({
    url: `goods?query=${obj.query}&pagenum=${obj.pagenum}&pagesize=${obj.pagesize}`,
    method: "get",
  });
  return data;
};
// 删除商品
const delGoods = async (obj) => {
  console.log();
  let data = await http({
    url: `goods/${obj.id}`,
    method: "delete",
  });
  return data;
};

// 商品分类
const getGories = async (obj) => {
  let data = await http({
    url: `categories?type=${obj.type}&pagenum=${obj.pagenum}&pagesize=${obj.pagesize}`,
    method: "get",
  });
  return data;
};
// 二级数据列表
const getPrend = async (obj) => {
  let data = await http({
    url: `categories?type=${obj.type}`,
    method: "get",
  });
  return data;
};

// 查看商品
const getLookGoods = async (obj) => {
  let data = await http({
    url: `categories/${obj.cat_id}`,
    method: "get",
  });
  return data;
};
// 编辑商品分类
const getEditGoods = async (obj) => {
  let data = await http({
    url: `categories/${obj.cat_id}`,
    method: "put",
    data: {
      cat_name: obj.cat_name,
    },
  });
  return data;
};
// 删除商品
const getDelGoods = async (obj) => {
  let data = await http({
    url: `categories/${obj.id}`,
    method: "delete",
  });
  return data;
};
// 添加商品分类
const getAddGoods = async (obj) => {
  let data = await http({
    url: `categories`,
    method: "post",
    data: obj,
  });
  return data;
};
// 商品分类参数数据
const getParams = async (obj) => {
  let data = await http({
    url: `categories`,
    method: "get",
  });
  return data;
};
//  参数列表
const getAttributes = async (obj) => {
  let data = await http({
    url: `categories/${obj.id}/attributes?sel=${obj.sel}`,
    method: "get",
  });
  return data;
};
// 添加动态参数或者静态属性
const getAddAttributes = async (obj) => {
  let data = await http({
    url: `categories/${obj.id}/attributes`,
    method: "post",
    data: {
      attr_name: obj.attr_name,
      attr_sel: obj.attr_sel,
    },
  });
  return data;
};
// 查询商品参数
const getLookParams = async (obj) => {
  let data = await http({
    url: `categories/${obj.id}/attributes/${obj.attrId}?attr_sel=${obj.attr_sel}`,
    method: "get",
  });
  return data;
};
// 提交修改商品参数
const getSubmitParams = async (obj) => {
  let data = await http({
    url: `categories/${obj.id}/attributes/${obj.attrId}`,
    method: "put",
    data: {
      attr_name: obj.attr_name,
      attr_sel: obj.attr_sel,
    },
  });
  return data;
};
// 删除
const getDeltParams = async (obj) => {
  let data = await http({
    url: `categories/${obj.id}/attributes/${obj.attrid}`,
    method: "delete",
  });

  return data;
};
// 添加tag接口
const getTagList = async (obj) => {
  let data = await http({
    url: `categories/${obj.id}/attributes/${obj.attrId}`,
    method: "put",
    data: obj,
  });
  return data;
};
const getGoodsParamsMany = async (obj) => {
  let data = await http({
    url: `categories/${obj.id}/attributes?sel=${obj.sel}`,
    method: "get",
  });
  return data;
};
const getGoodsParamsOnly = async (obj) => {
  let data = await http({
    url: `categories/${obj.id}/attributes?sel=${obj.sel}`,
    method: "get",
  });
  return data;
};

const addGoods = async (obj) => {
  let data = await http({
    url: `goods`,
    method: "post",
    data: obj,
  });
  return data;
};

export {
  getGoodsList,
  delGoods,
  getGories,
  getLookGoods,
  getEditGoods,
  getDelGoods,
  getPrend,
  getAddGoods,
  getParams,
  getAttributes,
  getAddAttributes,
  getLookParams,
  getSubmitParams,
  getDeltParams,
  getTagList,
  getGoodsParamsMany,
  getGoodsParamsOnly,
  addGoods,
};
