import axios from 'axios';

// 引入element js文件
import { MessageBox, Message } from 'element-ui';

// 创建一个axiso的实列

const http = axios.create({
  //开发环境跨域-npm run dev ,需要在vue.config.js里配置代理跨域proxy
  //       baseURL:"/api",
  //线上环境跨域-npm run build。可用第三方代理或者让后台给你开跨域
  // baseURL:"https://bird.ioliu.cn/v1?url=http://ustbhuangyi.com/",
  //可统一配置其它属性
  // responseType: "json",
  // withCredentials: true, // 是否允许带cookie这些
  // headers: {
  //     "Content-Type": "application/x-www-form-urlencoded;charset=utf-8"
  // },
  // baseURL: 'http://127.0.0.1:8888/api/private/v1/', //配置公共的接口
  baseURL: process.env.VUE_APP_API,
  timeout: 5000, // 请求超时时间
});

http.interceptors.request.use(
  (config) => {
    // 在发送请求之前做某件事-若是有做鉴权token , 就给头部带上token
    //  console.log('发送中',config); //config就是axios里返回的promise对象
    if (localStorage.getItem('token')) {
      config.headers.Authorization = localStorage.getItem('token');
    }
    // console.log(config);
    return config;
  },
  (err) => {
    Promise.reject(err);
  }
);

// axios 响应拦截
// response 响应的意思
http.interceptors.response.use(
  // res 表示服务器返回的响应信息
  // 在这里我们可以对响应信息进行预处理
  // 也可以在这里对错误代码字典进处理
  // 大家经常性的会看到很多的错误码 500 404 403 -1  303
  (res) => {
    // switch (res.data.code) {
    //   case 500:
    //     console.log("服务器错误");
    //     break;
    //   case 404:
    //     console.log("请求资源找不到");
    //     break;
    // }
    // console.log(res);s
    // res为promise 数据
    // 拿到token失效响应拦截的信息 看后台 返回的是状态码是多少
    const msg = res.data.meta.msg; // 拿到状态码信息
    const code = res.data.meta.status; // 账号的判断
    if (msg == '无效token') {
      MessageBox('token过期了,返回登录页面重新登录!!!', '系统提示', {
        confirmButtonText: '确定',
        cancelButtonText: '取消',
        type: 'warning',
      })
        .then(() => {
          location.href = '/home';
        })
        .catch(() => {
          MessageBox({
            type: 'info',
            message: '取消',
          });
        });

      // 全部提醒用户账号密码错误
    } else if (code !== 200) {
      Message({
        message: msg,
        duration: 1000,
        type: 'error',
      });
    }

    return res.data;
  },

  (err) => {
    // 判断没有链接服务器时的操作
    if ('Error: Network Error') {
      Message({
        message: '服务器崩溃,请稍后再试',
        duration: 1000,
        type: 'error',
      });
    }
    Promise.reject(err);
  }
);

// 抛出去
export default http;
