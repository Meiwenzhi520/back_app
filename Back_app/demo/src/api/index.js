import http from "./http";

// 获取登录数据
const mylogin = async (obj) => {
  let data = await http({
    url: "login",
    method: "POST",
    data: obj,
  });

  return data;
};
// 获取左边导航
const mymenus = async (obj) => {
  let data = await http({
    url: "menus",
    method: "get",
    data: obj,
  });

  return data;
};
// 获取用户列表
const myusers = async (obj) => {
  let data = await http({
    url: `users?query=${obj.query}&pagenum=${obj.pagenum}&pagesize=${obj.pagesize}`,
    method: "get",
  });
  return data;
};
// 添加
const addUsers = async (obj) => {
  let data = await http({
    url: "users",
    method: "post",
    data: obj,
  });
  return data;
};

// 删除
const delUsers = async (obj) => {
  console.log();
  let data = await http({
    url: `users/${obj.id}`,
    method: "delete",
  });
  return data;
};
// 查看
const lookUsers = async (obj) => {
  let data = await http({
    url: `users/${obj.id}`,
    method: "get",
  });
  return data;
};
// 修改
const editUsers = async (obj) => {
  let data = await http({
    url: `users/${obj.id}`,
    method: "put",
    data: {
      email: obj.email,
      mobile: obj.mobile,
    },
  });
  return data;
};

// 修改用户状态
const editStatus = async (obj) => {
  let data = await http({
    url: `users/${obj.id}/state/${obj.mg_state}`,
    method: "put",
  });
  return data;
};

// 所有角色列表
const allRoles = async () => {
  let data = await http({
    url: `roles`,
    method: "get",
  });
  return data;
};
// 分配用户的角色
const setRoles = async (obj) => {
  let data = await http({
    url: `users/${obj.id}/role`,
    method: "put",
    data: {
      rid: obj.rid,
    },
  });
  return data;
};
export {
  mylogin,
  mymenus,
  myusers,
  addUsers,
  lookUsers,
  editUsers,
  delUsers,
  editStatus,
  allRoles,
  setRoles,
};
