//  订单管理的接口
import http from "./http";

// 订单数据列表
const getorders = async (obj) => {
  let data = await http({
    url: `orders?query=&pagenum=${obj.pagenum}&pagesize=${obj.pagesize}`,
    method: "get",
    data: obj,
  });
  return data;
};
// 查看订单

const lookorders = async (obj) => {
  let data = await http({
    url: `orders/:id`,
    method: "get",
  });
  return data;
};

// 数据统计图

const getReports = async (obj) => {
  let data = await http({
    url: `reports/type/1`,
    method: "get",
  });
  return data;
};
export { getorders, lookorders, getReports };
