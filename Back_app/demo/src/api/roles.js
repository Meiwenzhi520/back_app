//  权限管理的接口
import http from "./http";

// 角色列表
const getroles = async (obj) => {
  let data = await http({
    url: `roles`,
    method: "get",
  });
  return data;
};
// 添加角色
const addRoles = async (obj) => {
  let data = await http({
    url: `roles`,
    method: "post",
    data: {
      roleName: obj.roleName,
      roleDesc: obj.roleDesc,
    },
  });
  return data;
};

// 查看角色
const lookRoles = async (obj) => {
  let data = await http({
    url: `roles/${obj}`,
    method: "get",
  });
  return data;
};

// 修改角色
const editRoles = async (obj) => {
  let data = await http({
    url: `roles/${obj.roleId}`,
    method: "put",
    data: {
      roleName: obj.roleName,
      roleDesc: obj.roleDesc,
    },
  });
  return data;
};
// 删除角色
const delRoles = async (obj) => {
  let data = await http({
    url: `roles/${obj}`,
    method: "delete",
  });
  return data;
};

// 权限管理
const getrights = async (obj) => {
  let data = await http({
    url: `rights/tree`,
    method: "get",
  });
  return data;
};
// 角色授权
const getPower = async (obj) => {
  let data = await http({
    url: `roles/${obj.roleId}/rights`,
    method: "post",
    data: {
      rids: obj.rids,
    },
  });
  return data;
};
// 权限列表
const getrightsList = async (obj) => {
  let data = await http({
    url: `rights/list`,
    method: "get",
  });
  return data;
};

// 删除角色指定得权限

const delAssignRoles = async (obj) => {
  let data = await http({
    url: `roles/${obj.roleId}/rights/${obj.rids}`,
    method: "delete",
  });
  return data;
};

export {
  getroles,
  addRoles,
  lookRoles,
  editRoles,
  delRoles,
  getrights,
  getPower,
  getrightsList,
  delAssignRoles,
};
