import Vue from 'vue';
import App from './App.vue';
import router from './router';
import store from './store';
import 'default-passive-events'; // 去除element警告

// 引入element
import ElementUI from 'element-ui';
import 'element-ui/lib/theme-chalk/index.css';
// 引入初始化样式
import './assets/css/reset.css';
Vue.use(ElementUI);

Vue.config.productionTip = false;

// 引入外部的样式
import TreeTable from 'vue-table-with-tree-grid';
Vue.component('tree-table', TreeTable);

import VueQuillEditor from 'vue-quill-editor';
Vue.use(VueQuillEditor);
// 文库域的样式
import 'quill/dist/quill.core.css'; // import styles
import 'quill/dist/quill.snow.css'; // for snow theme
import 'quill/dist/quill.bubble.css'; // for bubble theme

new Vue({
  router,
  store,
  render: (h) => h(App),
}).$mount('#app');

//{{ date | dateFormat('yyyy-mm-dd') }} 不填默认 yyyy-mm-dd hh-mm-ss
// 过滤器， 进行时间的格式化
Vue.filter('dateFormat', function(dateStr, pattern = '') {
  let dt = new Date(dateStr);
  let y = dt.getFullYear();
  let m = (dt.getMonth() + 1).toString().padStart(2, '0');
  let d = dt
    .getDate()
    .toString()
    .padStart(2, '0');
  pattern.toLowerCase(); //传入参数转成小写
  if (pattern.toLowerCase() === 'yyyy-mm-dd') {
    return `${y}-${m}-${d}`;
  } else {
    let hh = dt
      .getHours()
      .toString()
      .padStart(2, '0');
    let mm = dt
      .getMinutes()
      .toString()
      .padStart(2, '0');
    let ss = dt
      .getSeconds()
      .toString()
      .padStart(2, '0');
    return `${y}-${m}-${d} ${hh}:${mm}:${ss}`;
  }
});
