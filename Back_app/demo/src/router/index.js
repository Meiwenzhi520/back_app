import Vue from "vue";
import VueRouter from "vue-router";
import Home from "../views/Home.vue";

Vue.use(VueRouter);

const routes = [
  // 登录页面
  {
    path: "/home",
    name: "home",
    component: () => import("../views/Home.vue"),
  },
  // 首页填坑
  {
    path: "/index",
    name: "index",
    redirect: "/MyStart", //重定向第一个开始的页面
    component: () => import("../views/Index.vue"),
    children: [
      // 欢迎页面
      {
        path: "/MyStart",
        name: "MyStart",
        component: () => import("../views/user/MyStart.vue"),
      },
      // 用户管理
      {
        path: "/users",
        name: "users",
        component: () => import("../views/user/users.vue"),
      },
      // 权限管理
      {
        path: "/roles",
        name: "roles",
        component: () => import("../views/user/roles.vue"),
      },
      {
        path: "/rights",
        name: "rights",
        component: () => import("../views/user/rights.vue"),
      },
      // 商品列表
      {
        path: "/goods",
        name: "goods",
        component: () => import("../views/user/goods.vue"),
      },
      // 商品添加
      {
        path: "/addgoods",
        name: "addgoods",
        component: () => import("../views/user/addgoods.vue"),
      },
      // 商品分类
      {
        path: "/categories",
        name: "categories",
        component: () => import("../views/user/categories.vue"),
      },
      // 分类参数
      {
        path: "/params",
        name: "params",
        component: () => import("../views/user/params.vue"),
      },
      // 订单列表
      {
        path: "/orders",
        name: "orders",
        component: () => import("../views/user/orders.vue"),
      },
      // 数据统计
      {
        path: "/reports",
        name: "reports",
        component: () => import("../views/user/reports.vue"),
      },
    ],
  },
  // 全局定义开始的页面
  {
    path: "*",
    redirect: "/home",
  },
];

const router = new VueRouter({
  routes,
});

// 路由守卫
// 只能登录了才能访问页面 否则只能登录
router.beforeEach((to, form, next) => {
  if (!localStorage.getItem("token")) {
    if (to.path !== "/home") {
      return next("/home");
    }
  }
  next();
});
export default router;
