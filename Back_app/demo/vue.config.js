module.exports = {
  //配置输出路径
  publicPath: './',
  //引导webpack去找我的入口文件，即不同的main.js
  chainWebpack: (config) => {
    config.when(process.env.NODE_ENV === 'production', (config) => {
      config
        .entry('app')
        .clear()
        .add('./src/main-prod.js');
      //生产模式下打包时,把第3方插件排除在外不打包,这样可减少chunk文件的体积，提升性能。可把第3方插件放在打包后的index.html里，通过链接方式引入。不过使用externals属性要注意的是，虽然可以优化首屏加载速度，但是由于静态资源分离，也会增加http请求数量。所以如果是小项目，最好就不要用externals属性，因为小项目打包的出来的vender.js体积不大，建议项目体量较大的项目再用比较合适。

      //说一下这里的键值对配置，key名vue是包名,这个值是import from 'vue'时用的名称。value值是别名,是你在项目中用的别名
      //参考文章  https://www.jianshu.com/p/f6b3f097a56d
      config.set('externals', {
        vue: 'Vue',
        'element-ui': 'ELEMENT', // element
        echarts: 'echarts', // 统计视图
        lodash: '_', //lodasg
        'vue-quill-editor': 'VueQuillEditor', // quill 富文本
      });
      config.plugin('html').tap((args) => {
        return args;
      });
    });
    config.when(process.env.NODE_ENV === 'development', (config) => {
      config
        .entry('app')
        .clear()
        .add('./src/main.js');
      config.plugin('html').tap((args) => {
        return args;
      });
    });
  },
  //去除生产环境的代码镜像-productionSourceMap
  productionSourceMap: false,
};
